let Index = {
    name:'Index',
    data:()=>{
        return{
            TotalCount:15,
            FlowCount:211,
            time: '',
            year:'',
            month:'',
            date:'',
            WeekZh:'',
            WeekEn:'',
            day:'',
            hour:'',
            minute:'',
            second:'',
            ampm:''
        }
    },
    created() {
        setInterval(()=>{
            this.getTSNSData()
        },1000)
    }
    ,
    mounted() {
        setInterval(() => {
            const now = new Date();
            // 获取年月日星期
            this.year = now.getFullYear();
            this.month = now.getMonth() + 1;
            this.date = now.getDate();
            this.WeekEn = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][now.getDay()];
            this.WeekZh = ['星期天', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'][now.getDay()];
            // this.date = `${year}-${month}-${date} ${day}`;
            // // 获取时分秒和AM/PM
            this.hour = now.getHours()>12? now.getHours():now.getHours() % 12 || 12

            // this.hour = now.getHours() % 12 || 12;
            this.minute = now.getMinutes().toString().padStart(2, '0');
            this.second = now.getSeconds().toString().padStart(2, '0');
            this.ampm = now.getHours() < 12 ? 'AM' : 'PM';
            // this.time = `${hour}:${minute}:${second} ${ampm}`;
        }, 1000);
    },
    components:{
 
    },
    methods: {
       async getTSNSData(){
        fetch('http://localhost:9001')
        .then(response => {
            if (!response.ok) {
            throw new Error('Network response was not ok');
            }
            return response.json(); // 如果响应成功，则解析JSON数据
        })
        .then(data => {
            console.log(data); // 把解析后的数据展示到控制台
        })
        .catch(error => {
            console.error(error);
        });
       }
    },
    template:`
        <div class="container">
            <img src="./img/1.png" alt="" class="service">
            <img src="./img/2.png" alt="" class="toilet">
            <img src="./img/3.png" alt="" class="listener">
            <img src="./img/7.png" alt="" class="talLine">

            <div class="ToiletIconBox">
                <img src="./img/4.png" alt="" >
                <span>男厕</span>
            </div>

            <div class="InfoBox">
                <div class="NoUseCount"></div>
                <div class="TotalPos">
                    总厕位：{{TotalCount}}
                </div>
                <div class="PassengerFlow">
                    <div class="FlowLabel">客流</div>
                    <div class="FlowCount">{{FlowCount}}</div>
                    <div class="Unit">人</div>
                </div>
            </div>

            <div class="DateBox">
                <div class="Years-Month-Day">
                    <div class="Years">{{year}},</div>
                    <div class="Month">{{month}},</div>
                    <div class="Day">{{date}}</div>
                </div>
                <div class="Week-WeekEn">
                    <div class="Weekzh">{{WeekZh}}</div>
                    <div class="WeekEn">{{WeekEn}}</div>
                </div>
                <div class="Am-H-M-S">
                    <div class="AM">{{ampm}}</div>
                    <div class="H">{{hour}}:</div>
                    <div class="M">{{minute}}:</div>
                    <div class="S">{{second}}</div>
                </div>
            </div>

            <img src="./img/6.png" alt="" class="pafline-top">
            <img src="./img/5.png" alt="" class="FourBox">
            <img src="./img/6.png" alt="" class="pafline-middle">
        </div>
           
    `
}

const router = new VueRouter({
    routes: [
        { 
            path: '/', 
            component: Index
        },
    ],
    duplicateNavigationPolicy: 'ignore',
  })
const app =new Vue({
    router,
    silent: true,
})
app.$mount('#app') //挂载到id为app的元素上
