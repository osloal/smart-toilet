axios.defaults.baseURL = 'http://localhost:9001';
// axios.defaults.headers.common['Token'] ='eyJhbGciOiJSUzI1NiIsImtpZCI6IjZBQ0FGMTg4OUVGOENGN0Q2RDM5Q0Y4OTE2MUQ5NEFCIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2ODA3NjE2MTMsImV4cCI6MTY4MDc2MTkxMywiaXNzIjoiaHR0cHM6Ly9wYXNzcG9ydC5zaGl4aWFuamlhLmNvbSIsImF1ZCI6IkhvdXNlX0FwcCIsIm5vbmNlIjoiNGJlZjU3MTQxOWQ1NDkyMzg4MDViN2U3NTBmZDA2ZGEiLCJpYXQiOjE2ODA3NjE2MTMsImF0X2hhc2giOiJHZVVPMHNZYnl2c1NrbFlxZXpZZWxBIiwic19oYXNoIjoiOVBCci1UVXctOGpoMXhfVXQ1cEJoZyIsInNpZCI6Ijk1QkMxNTY3QzU3QkQ4RTIwOUQ5M0JFNTBFODg2MTZGIiwic3ViIjoiNTEyZjNiNjctYmIxOC1mNjEwLWMyNDEtM2EwNGJmM2JiODlkIiwiYXV0aF90aW1lIjoxNjgwNzYxNjEzLCJpZHAiOiJsb2NhbCIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL2dpdmVubmFtZSI6Im9zbG8iLCJ1bmlxdWVfbmFtZSI6InN4al8xNTgxNTU3NzQiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzeGpfMTU4MTU1Nzc0IiwiZ2l2ZW5fbmFtZSI6Im9zbG8iLCJuYW1lIjoic3hqXzE1ODE1NTc3NCIsInNob3dfd2VsY29tZSI6IjEiLCJhdmF0YXIiOiJodHRwczovL3N4ai1wcm9maWxlLm9zcy1jbi1zaGVuemhlbi5hbGl5dW5jcy5jb20vaG9zdC9kZWZhdWx0aW1nL2RlZmF1bHRhdmF0YXIucG5nIiwibmlja25hbWUiOiJvc2xvIiwiYWNjb3VudF90eXBlIjoiMSIsInNob3BpZCI6IjQ1IiwicWFpbmZvaWQiOiJkMDMyNDY1Zi1iM2E5LTA0MWMtNDRmMy0zYTA0YmYzYzhmYzQiLCJhbXIiOlsicHdkIl19.CbSWLCUyO3zJ-u3Ib1_uUQZjona3VsgwIHnuVUZAV7CRo2v9g249Anb2Xc6qD-RB2FBVGnScB3oqp96GIcMUJXFsg5y8c_7MDhGnd2tzRjlWdHOhNId75PRi2YZkjPJpftZO86HIrnQ4kzhbVnwl93SzZsB6R9MwYKj4p5IkpeZNmbSHe0L-tVwwZvFcpzOZoRrGxPlnadHWSE8TFhHSuspku2j8yDx9aXV6Bzt14Xp9iX7KsFZLC-LpXNh2uOU7rxvPlsdb6l-ESxfECTSS06E3lNZ_7WJbphc2PCCYvOias0p0dv_zBQyGz3b9-3RsVVhWlil9PQ1hCLxr90kK8A';
axios.defaults.headers.post['Content-Type'] = 'application/json';
const request = (url,option) => {
    return new Promise((resolve, reject) => {
        axios({
            method:option.method,
            url:url? url :'',
            data:option.data ? option.data :{},
        }).then(res =>{
                resolve(res)
        }).catch(err => {
            reject(err)
        });
    })
}