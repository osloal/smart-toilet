
		var container,  controls;
		var camera, scene, renderer,light;
        var model;

			init();
			animate();

			function init() {

				

				container = document.createElement( 'div' );
				document.body.appendChild( container );

				camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 0.25, 20000 );
				camera.position.set( - 180, 90, 180 );

				scene = new THREE.Scene();

				let texture = new THREE.TextureLoader().load('./3D/src/model/bg.png');

				scene.background = texture;

                var mAmbientLight = new THREE.AmbientLight(0xffffff);
				mAmbientLight.intensity = 0.8;
				mAmbientLight.position.set(0, 0, 800); 								
				scene.add(mAmbientLight);

				// model
				var loader = new THREE.FBXLoader();
				loader.load( './3D/src/model/man.fbx', function ( object ) {
					console.log(object)
					
					object.traverse( function ( child ) {

						// if ( child.isMesh ) {
						// 	// let tMaterial = new THREE.MeshLambertMaterial({})
						// 	// tMaterial.copy(child.material)
						// 	// child.material = tMaterial
						// 			if ( child.isMesh ) {
                        //         // if(child.name=='RED-1')
                        //         console.log(child.name)
						// 	}

						// }

					} );
					model = object;
					model.scale.set(0.01,0.01,0.01)
					scene.add( model );
					const box = new THREE.Box3().setFromObject(model);
					const center = box.getCenter(new THREE.Vector3());
					model.position.sub(center);
					model.rotation.x = -Math.PI / 2;
				} );
				// } );

				renderer = new THREE.WebGLRenderer( { antialias: true } );
				renderer.setPixelRatio( window.devicePixelRatio );
				renderer.setSize( window.innerWidth, window.innerHeight );
				// renderer.gammaOutput = true;
				// 渲染器开启阴影
				container.appendChild( renderer.domElement );

				controls = new THREE.OrbitControls( camera, renderer.domElement );
				controls.target.set( 0, - 0.2, - 0.2 );
				//是否开启右键拖拽
      			controls.enablePan = true;
				controls.update();

				window.addEventListener( 'resize', onWindowResize, false );
		
				// 坐标轴辅助线 x-red y-green z-blue
    			scene.add(new THREE.AxesHelper(1200));
			}

			function onWindowResize() {
				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();
				renderer.setSize( window.innerWidth, window.innerHeight );
			}

			function animate() {
				requestAnimationFrame( animate );
				renderer.render( scene, camera );
			}



